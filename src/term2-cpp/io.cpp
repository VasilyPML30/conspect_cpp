\documentclass[11pt]{article}
\usepackage{cmap}
\usepackage[T2A]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[russian]{babel}
\usepackage{graphicx}
\usepackage{amsthm,amsmath,amssymb}
\usepackage[russian,colorlinks=true,urlcolor=red,linkcolor=blue]{hyperref}
\usepackage{enumerate}
\usepackage{datetime}
%\usepackage{minted}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{color}
\usepackage{verbatim}
\usepackage{color}
\usepackage{listings} 

\usepackage{caption}
\DeclareCaptionFont{white}{\color{white}}
\DeclareCaptionFormat{listing}{\colorbox{gray}{\parbox{\textwidth}{#1#2#3}}}
\captionsetup[lstlisting]{format=listing,labelfont=white,textfont=white}

\begin{document}

\lstset{ %
language=C,                 % выбор языка для подсветки (здесь это С)
basicstyle=\small\sffamily, % размер и начертание шрифта для подсветки кода
numbers=left,               % где поставить нумерацию строк (слева\справа)
numberstyle=\tiny,           % размер шрифта для номеров строк
stepnumber=1,                   % размер шага между двумя номерами строк
numbersep=5pt,                % как далеко отстоят номера строк от подсвечиваемого кода
backgroundcolor=\color{white}, % цвет фона подсветки - используем \usepackage{color}
showspaces=false,            % показывать или нет пробелы специальными отступами
showstringspaces=false,      % показывать или нет пробелы в строках
showtabs=false,             % показывать или нет табуляцию в строках
frame=single,              % рисовать рамку вокруг кода
tabsize=2,                 % размер табуляции по умолчанию равен 2 пробелам
captionpos=t,              % позиция заголовка вверху [t] или внизу [b] 
breaklines=true,           % автоматически переносить строки (да\нет)
breakatwhitespace=false, % переносить строки только если есть пробел
escapeinside={\%*}{*)}   % если нужно добавить комментарии в коде
}

\section{Ввод-вывод в С++}
В языке C для чтения из файла и из консоли использовались разные функции. 
\begin{lstlisting}
FILE* f = fopen(``infile.txt'', ``r'');
int rf;
fscanf(f, ``%d'', &rf);
int rc;
scanf(``%d'', &rc);
\end{lstlisting}
В C++ мы хотим добавить уровень абстракции, чтобы ввод-вывод для всего был одинаковый. Эта абстракция - потоки (не thread, а stream!).
Самый простой пример потока ввода-вывода - консольные std$::$cin и std$::$cout.
\subsection{Иерархия классов}
Все потоки - наследники класса {\bf ios} (он сам наследуется от {\bf ios\_base}), от {\bf ios} наследуются {\bf istream} и {\bf ostream} - потоки для ввода и вывода соответственно.\\
 От {\bf istream}  наследуются {\bf ifstream}  и {\bf istringstream}  - потоки для чтения из файлов и строк. \\
 От {\bf ostream}, соответственно, {\bf ofstream} и {\bf ostringstream} - для записи в файл/строку. \\
 Также есть  {\bf iostream}, который наследуется от {\bf istream} и от {\bf ostream}, он поддерживает как чтение, так и запись. Его наследники - {\bf fstream} и {\bf stringstream}, они делают, не поверите, чтение и запись в файл или строку. Внутри практически не отличается от родительских классов, но рекомендуется использовать  {\bf istream} или {\bf ostream}, если нужно либо только читать, либо только писать - так меньше возможностей выстрелить себе в ногу.
 \subsection{Методы/флаги/манипуляторы }
 \subsubsection{Методы для чтения/записи в консоль}
 Самый простой пример потока ввода-вывода - консольные std$::$cin и std$::$cout.
\begin{lstlisting}
int r1, r2;
std::cin >> r1 >> r2;
std::cout << ``We have just read `` << r1 << ``and `` << r2 << std::endl;
std::cerr << ``Some console log output\n'';
\end{lstlisting}
cin - объект класса istream. Он считывает из стандартного потока ввода (по умолчанию это консоль) значения нужного нам типа. Можно считывать несколько значений сразу, разделяя их ``>>''. \\
cout, объект класса ostream, осуществляет запись в стандартный поток ввода. Можно выводить несколько объектов, разделяя их ``<<''. \\
Вывод буфферизованный, а значит, производится сразу же. Если хотим насильно опустошить буфер, можем вызвать cout.flush(), можем вывести std::endl (это перевод строки с очисткой буфера). А поток std::cerr не буфферизуется.
 \subsubsection{Работа с файлами}
 Чтобы работать с файлами, сначала нам надо открыть их на чтение или запись (или на оба). Мы делаем это, когда вызываем конструктор потока, либо командой open. Закрываем файл командой close.
\begin{lstlisting}
int a, b;
std::ifstream fin(``input1.txt'');
fin >> a;
fin.close();
fin.open(``input2.txt'');
fin >> b;
std::ofstream fout(``output.txt'');
fout << a + b;
\end{lstlisting}
Файл сам закрывается в деструкторе, который вызовется, когда мы выйдем из нашего scope (функция, цикл, условие - все, что в фигурных скобочках), в котором создали поток. Не надо закрывать его руками без надобности! Хоть ничего и не сломается, это будет не по RAII. \\
Файл можно открывать в разных режимах:
\begin{enumerate}
  \item ios\_base$::$in - на ввод
  \item ios\_base$::$out - на вывод
  \item ios\_base$::$ate - при открытии переместить указатель в конец файла
  \item ios\_base$::$app - открыть файл для записи в конец файла
  \item ios\_base$::$trunc -  удалить содержимое файла, если он существует
  \item ios\_base$::$binary - открыть файл в двоичном режиме
\end{enumerate}
Эти флаги по сути являются битовыми масками, поэтому их можно комбинировать с помощью ``|'' вот так:
\begin{lstlisting}
std::ifstream fin(``input.bin'', 
                  ios_base::in | ios_base::binary);
\end{lstlisting}
Когда мы открываем файл в двоичном режиме, для чтения нужно использовать метод read, который есть у нашего потока. Ему потребуется буфер, куда он будет записывать считанные байты и количество байт, которые он будет считывать.
\begin{lstlisting}
fin.read(buffer, 1);
cout << buffer[0];
\end{lstlisting}
Забавный факт: read возвращает istream\&, так что мы можем писать конструкции типа fin.read(...).read(...).read(...) \\
Для записи то же самое:
\begin{lstlisting}
std::ofstream fout(``output.bin'', 
                  ios_base::out | ios_base::binary);
fout.write(buffer, 1);
\end{lstlisting}
tellg возвращает позицию в файле, seekg перемещает на заданную позицию в файле. eof возвращает, достигли ли мы конца файла или нет.
 \subsubsection{Манипуляторы}
А еще есть манипуляторы. Их можно выводить в поток, и они будут что-то менять (подключите библиотеку iomanip). Например, 
\begin{enumerate}
  \item std$::$hex - вывод в шестнадцатеричной системе счисления
  \item std$::$flush - выводит содержимое буфера
  \item std$::$endl - переводит строку и делает flush
  \item std$::$setprecision(p) - устанавливает число цифр после запятой
\end{enumerate}
\begin{lstlisting}
std::cout << std::hex << 15 << std::endl; //same as``f'\n' and flush
\end{lstlisting}
\subsection{Обработка ошибок}
Потоки предоставляют нам множество возможностей выявления и обработки ошибок. \\
У потоков определён оператор bool, позволяющий, например, в форме ``if (!fin)'' узнать, смогли ли мы открыть файл.
Точно так же ``if (cin >> x)'' ответит про конкретную операцию считывания. \\
Больше деталей доступно при вызове методов:
\begin{enumerate}
  \item good() - вернет true, если всё в порядке
  \item bad() - если возникла фатальная ошибка
  \item fail() - если произошла неудачная операция с потоком (например, попытались считать слово в int)
  \item eof() - если достигли конца файла.
\end{enumerate}
С теми же деталями мы можем ловить исключения. Для этого потоку необходимо задать, какие исключения он будет кидать (в формате битмасок).
\begin{lstlisting}
std::ifstream fin;
file.exceptions(std::ifstream::failbit | std::ifstream::badbit); 
int x;
try {
  fin.open ("test.txt");
  fin >> x;
}
catch (std::ifstream::failure e) {
  std::cerr << "Exception opening/reading/closing file\n";
}
\end{lstlisting}
\subsection{Ввод-вывод пользовательских типов}
Пусть у нас есть свой тип и мы хотим, чтобы потоки могли его считывать и выводить. \\
Для этого нам нужно будет перегрузить оператор ``>>'' и/или ``<<''.
При этом если мы планируем выводить приватные поля класса или вызывать приватные методы, нужно будет также объявить оператор как friend. \\
Пример:
\begin{lstlisting}
class MyClass {
private:
  int value;
public:
  friend ostream& operator<<(ostream& os, const MyClass& mc);
  friend istream& operator>>(istream& is, MyClass& mc);
};

ostream& operator<<(ostream& os, const MyClass& mc) {
  os << mc.value;
  return os;
}

istream& operator>>(istream& is, MyClass& mc) {
  is >> mc.value;
  return is;
}
\end{lstlisting}
Заметим, что мы возвращаем тот же поток, чтобы поддерживать ввод/вывод многих объектов через несколько ``>>''/``<<''
\end{document}
