========std::vector
reserve() -- влияет на capacity
resize() -- влияет на size и на capacity
Допустим, у нас много push_back(), а потом много pop_back().
После этого мы неэффективно расходуем память.
С++11: shrink_to_fit();
1.
f(MyArray a) {}
f(MyArray(10)); // всё норм
2.
f(MyArray &a) {}
f(MyArray(10)); // не скомпилится, потому что мы будем пытаться редактировать временную переменную.
3.
f(const MyArray &a) {}
f(MyArray(10)); //норм, мы не редактируем временную переменную.
vector<int>(arr).swap(arr) //конструируем временный объект, как копию arr, а затем присваеваем все поля временного вектора вектору arr. После этого анонимная переменная умрёт.

=====Если хотим из vector<int> в int* :
void f(int *array, size_t n) {}
vector<int> v(10);
f(&v[0], v.size());
f(&(*v.begin()), v.size());

=====std::string
vector<char>
s = "Hello";
s += ", world";
до С++11 cow -- copy on write:
string s1 = s;// вгутри будет chared_ptr, то есть s1 и s будут указывать на одну и ту же строчку.
Да:
f(string s) {}
f("Hello");
f(string("Hello"));
Нет:
f(string &s) {}
f("Hello");
f(string("Hello"));
Да:
f(const string &s) {}
f("Hello");
f(string("Hello"));

Методы посмотрите сами в хелпе.

const char* string::c_str() // указатель на строчку, терминированную нулём.
const char* string::data() // не обязательно терминировано нулём.
getline(istream???, string) // обычное считывание.
getline(istream???, string, "\n") // считывание до перевода строки

====Требования к типам в контейнерах
vector<T>
T: 1. copy constructable
   2. assignable

Последовательные контейнеры:
string,
vector,
list,
deque.

Адапторы:
stack,
queue,
priority_queue.
stack<T, container = deque>

Ассоциативные контейнеры
1. сбалансированное дерево поиска:
  set, multiset, map, multimap.

set<int> s;
Множество. Хранит каждый эллемент в однов экземпляре
//s.size() == 0;
s.insert(10);
s.insert(20);
s.insert(10);
//s.size() == 2;
template <typename F, typename S>
pair {
  F first;
  S second;
};

insert возвращает pair<set<int>::iterator, bool> -- итератор на добавляемый элемент и флаг, был ли он добавлен.
У хранимого класса должен быть оператор <.
set<int>::iterator it = s.find();
*it не изменяемый, потому что если поменять, структура дерева нарушится.
for (set<int>::iterator it = s.begin(); it != s.end(); ++it);
Пройдет всё множество в отсортированном порядке.
++it работает быстрее чем it++, потому что он не создает промежуточного объекта.

multiset:
s.count(const T& elem) {} -- сколько раз встречается элемент

map:
Такое же дерево, но хранит оно пары -- <ключ, значение>

map<string, int> p;
p.insert(pair<string, int>("Vasya", 123));
p.insert(make_pair("Vasya", 123)); // выведет типы сама
Упражнение: написать реализацию make_pair().
p["Vasya"] = 123;
Неприятный эффект:
int a = p["Kolya"]; // создаст элемент с ключом "Kolya" и значением по умоланию.

map<string, int>::iterator it = p.begin();
*it // pair<string, int>
*(it).first -- ключ
*(it).second -- значение

Пусть у класса Person опрератор < сравнивает объекты по именам, а мы хотим по возрасту
class by_age {
  bool operator()(const Person &p1, const Person &p2) {
    return p1.age < p2.age;
  }
};

by_age()(p1, p2) //создать временную переменную и вызвать у него оператор().
set<Person, by_age>