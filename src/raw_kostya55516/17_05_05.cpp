Сегодня отдыхаем!
В тест войдёт мув и лямбда, он будет без баллов

Предупреждение, тут в основном просто списано с презентации просто для удобства дальнейшей обработки

using - классная штука!

template <typename T>
T sum(T n) {return n;}

template<typename T, typename ...Args>
T sum(T n, Args... rest) {return n + sum(rest...);} // это рекурсия, которая развенётся в момент компиляции

double d = sum(3, (double)4.3, 5);

"..." - ощипывает один аргумент, затем тот же шаблон применяется ещё раз, пока не дойдём до базы "Шаблонной рекурсии" с одним аргументом T sum(T n)
для этого примера компилятор сгененрирует три функции:

T sum(T, Args ...) [with T = int; Args = {double, int}]
T sum(T, Args ...) [with T = double; Args = {int}]
T sum(T) [with T = int]


//В си юзается так!
void simple_printf(const char* fmt, .../*это синтаксис, а не лень*/) {// Он не будет во время компиляции понимать, какое там кол-во параметров, он просто засунет на стек всё, что было передано
	va_list args;
	va_start(args, fmt);
	while(*fmt != '\0') {
		if (*fmt == 'd') {
			int i = va_arg(args, int)//Если d - начиная с этого адреса копируй переменную размера инт по байтам, а потом увеличивает args на кол-во
			...//это лень, а не синтаксис
		}
	fmt++;
	}
	va_end(args);
}

//ошибки не будут замечены компилятором:
printf("%s", 5);
printf("%d %d", 4);
printf("%d", 5, 4);

Сделаем то же саоме, но с контролем типов или хотя бы проверкой в рантайме.

template<typename T, typename ...Args>
void printf(const char *s, T value, Args... rest) {
	while (*s) {
		if (*s == '%' && *(++s) != '%') {
			std::cout << value;
			printf(++s, rest...);
			return;
		}
		std::cout << *s++;
	}
	throw std::logic_error("extra arguments provided to printf");
}

делаем хэш!
class Point {
	private:
		int x, y;
	public:
		bool operator ==(const Point& rhs) const {
			return x == rhs.x && y == rhs.y;
		}
}
namespace std {
	template<>
	struct hash<Point> {
		size_t operator()(Point const & p) const {
			return (std::hash<int>()(p.getX()) * 51 + std::hash<int>()(p.getY()));
		}
	};
}

std::unordered_set<Point> points;

Потоки!

std::fuction

void execute(const vector<function<void ()>> &fs) {
	for (auto &f : fs)
		f();
}

struct functor {
	void operator()() const {
		cout << "functor" << endl;
	}
};

int main() {
	vector <function<void ()>> x;
	x.push_back(plain_old_func);
	
	functor functor_instance;
	x.push_back(functor_instance);

	x.push_back([] () {cout << "lambda" << endl; });
	execute(x);
}

std::bind 

void show_text(const string &t) {
	cout << "TEXT: " << t << endl;
}

int main() {
	std::vector<function<void ()>> x;
	function<void ()> f = bind(show_text, "Bound function");//Хочу функцию чтобы у неё как бы было ноль параметров и я мог положить это внутрь

	x.push_back(f);
	execute(x);
}

using namespace std::placeholders;
int multiply(int a, int b) {
	return a * b;
}

int main() {
	auto f = bind(multiply, 5, _1); //_1 - подставить первым аргументом
	cout << "out: " << f(6);
}

===========
std::thread

void f1(int n) {
	std::cout << "f1: " << n << std::endl;
}

void f2(int &n) {
	n++;
}

int main() {//три потока - один мэин, потом ещё два.
	int m = 45;
	std::thread t1(f1, m); //в первом потоке вызовется f1 и будет передано m по значению
	std::thread t2(f2, std::ref(m)); // тут будет вызвано по ссылке f2; зачем ref? чтобы std::function понял, что нужно сгенирировать функцию. Внутри этого треда как бы вызваетсяя std::function
	//Нужно явно указать, что это ссылка, ибо это всё шаблоны и компилятор должен понять, какую функцию генерировать
	t1.join();
	t2.join();// дождаться первого и второго потока
	cout << m;
}

=========

std::std::vector<std::thread> threads;

for(int i = 0; i < 5; i++) {
	threads.push_back(std::thread([](){
		std::cout << std::this_thread::get_id()
		<< std::endl;
	})); //http://cs5.pikabu.ru/post_img/2014/07/09/6/1404894891_1119265824.png
}

for (auto &thread : threads) {
	thread.join();
}