Приведение типов в стиле C++
============================

int a = 3;
char b = 0;
b = a;
 это присваивание в си.
Компилятор позволял присваивать любые типы, опасные ли не очень.
const int a = 3;
int *p = &a;
Программа будет успешно скомпилированна.
Чтобы не было предупреждения - надо сказать компилятору "Я всё контролирую": pi = (int*)pc;
double a = (double)1/5; чтобы не получить инт.

const char* s = "hello"; тут const будет уже в двоичном коде, 
если сделать так:
char *pc = s;
pc[0] = 0; - аварийно завершится, ибо память в read only.

f(const int *pi) {
	int *p = pi;
	p[0] = 1;
}

main() {
	char s[] = "hello";
	f(s);
}
Так ничего не сломается, но это не очень аккуратно

как итог - преобразование типа - штука опасная, хорошо бы эти места помечать.

С++
===
1.
int *pi;
char *pc;
pc = pi; // ERROR!
pc = (char*)pi;//cool

2.
void *pv;
pv = pi; //тут нет адресной арифметики, поэтому компилятор не будет ругаться

pi = (int*)pv; // без явного приведения будет плохо.

3. A <- B //B наследник класса A
  f()  f()
  	   g()

  B *pb = new B;
  A *pa = pb; // не нужен (A*), т.к. B является и A, все поля и методы имеются
  pa->g(); // ERROR: нет такого в А

Для обратной ситуации нужно явное преобразование.
---------------------------------------------
B *pb = new B;		|	A *pa = new A;
A *pa = pb;			|	B *pb1 = (B*)pa;
B *pb1 = (B*)pa;	| 	pb1->g(); //TRASH AND CRUSH!!
pb1->g() //OK		

Пользов.
class fraction_pair {
	int a;
	int b;
}

class fraction_array {
	int array[2];
}

main() {
	fraction_pair p(1, 1);
	double d = p;//без преобразований
	fraction_array a (2, 2);
	fraction_array pa = a; // тоже без приведений.
}

class fraction_pair {
	int a;
	int b;

	operator double() {
		return a*1.0/b;
	}

	fraction_pair(const fraction_array &arr) {
		a = arr[0];
		b = arr[1];
	}
}

double a = (double)pair;
Есть четыре преобразования типа 

static_cast<>
const_cast<>
reinterpret_cast<>
dynamic_cast<>

int *pi;
char *pc;
pc = static_cast<char*>(pi); // шаблонный параметр - то, к чему преобразоываем
Чем это особо лучше, чем в СИ? В принципе.. ничем.
Ничего нового не происходит, он так же вызывает ошибку, но в коде это сделать немного удобнее, плюс загрепить такое уже очень просто.
Если программа падает и возможно это связано с кастами, то можно найти все касты по "_cast"


static_cast<> - численные значения / наследование / пользовательские преобразования, типа operator double / в void*
Все те же проблемы остаются, если пользоваться неаккуратно, если классы никак не связаны отношением наследования - оно не скомпилится

const_cast<> - преобразования в/из const;

reinterpret_cast<> - преобразование указателей разных типов

B -> A
A *pa = static_cast<A*>pb;

1. size_t super_fast_strlen(char *s);

f(const char *s) {
	super_fast_strlen(s); //не скомпилируется
}

2. class AVL_Tree {
	const T& get (int index) const {
		... //много строчек
	}
	T& get(int index) {
		//напишем аккуратно без копирования
		return const_cast<T&>(const_cast<const AVL_Tree<T>*>(this)->get(index));
	}
}

Предположим, что сделан только без константы, тогда такой код:
f (const AVL_Tree<T> & tree) {
	T a = tree->get();
}

//пррв

3.
gsort* array;
char *pc = reinterpret_cast<char*>(array);


dynamic_cast<>
==============

A : f();
^
|
B : f(), g();

A* pA = new A;
B* pb = static_cast<B*> (pA);
pb->g(); // просто посмотрит, что там наследник, не скажет, что я дурак и всё полетит в тартарары
-------------------------
B *pb = dynamic_cast<B*> (pA); // он либо сделает каст, если это легально, либо сделает NULL
if (pb == NULL ) {
	cout << "ой ты дурак";
}
/*if (random)
	a* = new A
else
	a* = new B
if (dynamic_cast<B*>(a) != NULL)
	a->g()*/
Если делать из B в A, то всё будет ок.

Должны быть таблицы виртуальных функций, ибо нужно место, где хранится всякая фигня - таблица виртуальных функций

пусть есть на стеке
A a;
B b;

a = b; // он скопирует просто все поля, которые там есть.

По сути dynamic_cast<> делает лишь более сложные проверки, которые нельзя сделать на стадии компиляции.
Теперь давайте поймём, где это можно применить, когда мы наконец поняли синтаксис.

Есть у меня программа для рисования в векторном формате.

IShape ---> Square
|   |
v   ----> Circle
Rectangle


std::vector<IShape*> shapes;
for (...) {
	ISpahe *ps = shapes[i];
	if (dynamic_cast<Rectangle*>(ps)){ //type_traits<> вернуло бы Ishape
		сохранить в бинарный файл.
	} else {
		сохранить в текстовый файл.
	}
}
это всё: RTTI - realtime type identification 

#include <typeinfo>
/*WTF?
type_info typeid(..);
A *pA = new A;
type_info ti = typeid(*pA);
"A"  ti.name
ti == 
ti !=*/

Шаблонными функции не всегда обойдешься.

intersect (IShape *p, std::vector<IShape> v) {
	type_info ti1 = typeid(*p);
	for (...)
		type_info ti2 = typeid(*v[i]);
		if (ti1.name == "Square" && ti2.name == "Circle") {
			пересеки их
		} // во время работы узнает их тип и посмотрит имя.
} 