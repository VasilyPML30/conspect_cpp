Конспект по С++.
=================

### Необходимый софт
Помимо, собственно, *git* для работы с репозиторием, вам потребуется *XeTeX*
и *Python 3* для сборки конспектов ([почему именно XeTeX][5]). В Debian-подобном
дистрибутиве Linux для установки всего нужного будет достаточно команды:
```bash
sudo apt-get install git python3 texlive texlive-lang-cyrillic texlive-xetex texlive-generic-extra fonts-cmu
```
Что касается Windows, то XeTeX по умолчанию есть в составе MiKTeX.

### Сборка

Для сборки нужно пользоваться утилитой build.py, находящейся в корне репозитория.

На линуксе это должно выглядеть как
```bash
./build.py
```

А на виндоусе как:
```bash
python build.py
```

Скрипт build.py поддерживает очень много различных опций, которые можно задать
через консоль или файлы .config (общая конфигурация), .localconfig (локальная конфигурация).

### Структура директорий
*   __src/term2-cpp__
    Собственно конспект.
    
*   __pdf__
    Тут живут выходные файлы

*   __tmp__
    Директория для временных файлов. Когда build.py собирает конспект, всё содержимое папки конспекта
    копируется сюда и вызывается `xelatex`. Если всё собралось, то выходная pdf будет скопирована в
    соответствующую директорию.

    Также здесь можно прочитать логи `xelatex`-а, если конспект откажется собираться.
    
*   __.config__, __.localconfig__
    Конфигурация системы сборки в формате json. Найстройки .localconfig перезаписывают найстройки в .config

### Текстовые файлы
__Все__ текстовые файлы должны хранится в кодировке utf-8.

### Полезные ссылки
* [Git tutorial][1]
* [Документация GitLab][7]
* [TeX tutorial][2]
* [Главное отличие XeTeX: шрифты][6]
* [GitLab-flavored Markdown][3]
* [SEWiki][4]

[1]: http://git-scm.com/book/
[2]: http://www.latex-tutorial.com/tutorials/
[3]: http://docs.gitlab.com/ee/user/markdown.html
[4]: http://mit.spbau.ru/sewiki/
[5]: http://tex.stackexchange.com/questions/3393/what-is-xetex-exactly-and-why-should-i-use-it
[6]: http://mirror.macomnet.net/pub/CTAN/macros/latex/contrib/fontspec/fontspec.pdf
[7]: http://gitlab.com/help
